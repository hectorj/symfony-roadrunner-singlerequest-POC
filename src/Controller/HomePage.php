<?php
declare(strict_types=1);

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class HomePage extends Controller
{
    /**
     * @Route(path="/")
     */
    public function renderHomePage(): Response
    {
        return new Response('Hello World!');
    }
}
