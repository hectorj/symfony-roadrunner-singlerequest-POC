<?php

use App\Kernel;
use Spiral\Goridge\StreamRelay;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;
use Spiral\RoadRunner;

require __DIR__.'/../vendor/autoload.php';

// The check is to ensure we don't use .env in production
if (!isset($_SERVER['APP_ENV'])) {
    if (!class_exists(Dotenv::class)) {
        throw new \RuntimeException('APP_ENV environment variable is not defined. You need to define environment variables for configuration or add "symfony/dotenv" as a Composer dependency to load variables from a .env file.');
    }
    (new Dotenv())->load(__DIR__.'/../.env');
}

$env = $_SERVER['APP_ENV'] ?? 'dev';
$debug = (bool) ($_SERVER['APP_DEBUG'] ?? ('prod' !== $env));

if ($debug) {
    umask(0000);

    Debug::enable();
}

if ($trustedProxies = $_SERVER['TRUSTED_PROXIES'] ?? false) {
    Request::setTrustedProxies(explode(',', $trustedProxies), Request::HEADER_X_FORWARDED_ALL ^ Request::HEADER_X_FORWARDED_HOST);
}

if ($trustedHosts = $_SERVER['TRUSTED_HOSTS'] ?? false) {
    Request::setTrustedHosts(explode(',', $trustedHosts));
}

$relay = new StreamRelay(STDIN, STDOUT);
$psr7 = new RoadRunner\PSR7Client(new RoadRunner\Worker($relay));

$kernel = new Kernel($env, $debug);
while (true) {
    try {
        $kernel->boot();

        // We pre-instantiate a few services to have them ready when a request comes
        $kernel->getContainer()->get('doctrine');
        $kernel->getContainer()->get('event_dispatcher')->getListeners();

        $httpFoundationFactory = new HttpFoundationFactory();
        $psr7Factory = new DiactorosFactory();
        $psr7Request = $psr7->acceptRequest();
        if ($psr7Request === null) {
            break;
        }


        $request = $httpFoundationFactory->createRequest($psr7Request);

        $response = $kernel->handle($request);

        $psr7Response = $psr7Factory->createResponse($response);
        $psr7->respond($psr7Response);

        $kernel->terminate($request, $response);
        $kernel->shutdown();
    } catch (Throwable $e) {
        $psr7->getWorker()->error((string)$e);
    }
}
