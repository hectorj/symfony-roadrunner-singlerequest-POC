FROM php:7.2-cli

RUN curl -L https://github.com/spiral/roadrunner/releases/download/v1.0.1/roadrunner-1.0.1-linux-amd64.tar.gz | tar xz
RUN mv roadrunner-1.0.1-linux-amd64/rr /usr/bin/rr && rm -rf roadrunner-1.0.1-linux-amd64

VOLUME /app
WORKDIR /app

CMD ["rr", "serve", "-v", "-d"]
